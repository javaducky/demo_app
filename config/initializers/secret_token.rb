# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'ed9c3ccab49fc862d738a8c2bdb2908de7aaa9a5c83d93e2090820f6241883ade5ef393f18bfac44334c220b913b2c2244761e2b242b222031238fa8fa33ef93'
